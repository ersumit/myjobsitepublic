export interface User {
  UID: number;
  CompanyName: string;
  FName: string;
  LName: string;
  Password: string;
  Email: string;
  PhoneNo: string;
  UserType: string;
  Status: string;
  Active: number;
}
