import { BrowserModule } from "@angular/platform-browser";
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { IonicApp, IonicErrorHandler, IonicModule } from "ionic-angular";
import { SplashScreen } from "@ionic-native/splash-screen";
import { StatusBar } from "@ionic-native/status-bar";
import { NgxErrorsModule } from "@ultimate/ngxerrors";
import { Config } from "../config";
import { PreloadImage } from "../components/preload-image/preload-image";
import { BackgroundImage } from "../components/background-image/background-image";
import { ShowHideContainer } from "../components/show-hide-password/show-hide-container";
import { ShowHideInput } from "../components/show-hide-password/show-hide-input";
import { ColorRadio } from "../components/color-radio/color-radio";

import { MyApp } from "./app.component";
import { HomePage } from "../pages/home/home";
import { AngularFireModule } from "angularfire2";
import { AngularFireAuth } from "angularfire2/auth";
import { firebaseConfig } from "../config";
import { AuthService } from "../services/auth.service";
import { LoginPage } from "../pages/login/login";
import { SignupPage } from "../pages/signup/signup";
import { TextMaskModule } from "angular2-text-mask";
import { ForgotPasswordPage } from "../pages/forgot-password/forgot-password";
import { VerifyPhonePage } from "../pages/verify-phone/verify-phone";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    VerifyPhonePage,
    PreloadImage,
    BackgroundImage,
    ShowHideContainer,
    ShowHideInput,
    ColorRadio
  ],
  imports: [
    BrowserModule,
    NgxErrorsModule,
    TextMaskModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig.fire)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    ForgotPasswordPage,
    VerifyPhonePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AngularFireAuth,
    AuthService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {}
