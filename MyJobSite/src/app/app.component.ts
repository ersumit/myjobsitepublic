import { Component, ViewChild } from "@angular/core";
import { StatusBar } from "@ionic-native/status-bar";
import { SplashScreen } from "@ionic-native/splash-screen";
import { App, MenuController, Nav, Platform } from "ionic-angular";
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";
import { AuthService } from "../services/auth.service";

@Component({
  templateUrl: "app.html"
})
export class MyApp {
  pages;
  rootPage;
  private app;
  private platform;
  private menu: MenuController;
  @ViewChild(Nav) nav: Nav;

  constructor(
    app: App,
    platform: Platform,
    menu: MenuController,
    private statusBar: StatusBar,
    splashScreen: SplashScreen,
    private auth: AuthService
  ) {
    this.menu = menu;
    this.app = app;
    this.platform = platform;
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.initializeApp();

    // set our app's pages
    this.pages = [{ title: "Home", component: HomePage, icon: "home" }];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
    });

    this.auth.afAuth.authState.subscribe(
      user => {
        if (user) {
          this.rootPage = HomePage;
        } else {
          this.rootPage = LoginPage;
        }
      },
      () => {
        this.rootPage = LoginPage;
      }
    );
  }

  login() {
    this.menu.close();
    this.auth.signOut();
    this.nav.setRoot(LoginPage);
  }

  logout() {
    this.menu.close();
    this.auth.signOut();
    this.nav.setRoot(LoginPage);
  }

  openPage(page) {
    this.menu.close();
    this.nav.setRoot(page.component);
  }
}
