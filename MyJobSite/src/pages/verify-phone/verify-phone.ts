import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  AlertController,
  NavParams
} from "ionic-angular";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import firebase from "firebase";

/**
 * Generated class for the VerifyPhonePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var window;

@IonicPage()
@Component({
  selector: "page-verify-phone",
  templateUrl: "verify-phone.html"
})
export class VerifyPhonePage {
  verify_phone: FormGroup;
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  constructor(
    public nav: NavController,
    private auth: AuthService,
    public navParams: NavParams,
    public alertCtrl: AlertController
  ) {
    this.verify_phone = new FormGroup({
      PhoneNumber: new FormControl("", [Validators.required])
    });
  }

  ionViewDidLoad() {
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container"
    );
  }

  send(phoneNumber: number) {
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = "+" + this.verify_phone.value.PhoneNumber;
    this.auth
      .verifyPhone(phoneNumberString, appVerifier)
      .then(confirmationResult => {
        // SMS sent. Prompt user to type the code from the message, then sign the
        // user in with confirmationResult.confirm(code).
        let prompt = this.alertCtrl.create({
          title: "Enter the Confirmation code",
          inputs: [
            { name: "confirmationCode", placeholder: "Confirmation Code" }
          ],
          buttons: [
            {
              text: "Cancel",
              handler: data => {
                console.log("Cancel clicked");
              }
            },
            {
              text: "Send",
              handler: data => {
                confirmationResult
                  .confirm(data.confirmationCode)
                  .then(function(result) {
                    // User signed in successfully.
                    console.log(result.user);
                    // ...
                  })
                  .catch(function(error) {
                    // User couldn't sign in (bad verification code?)
                    // ...
                  });
              }
            }
          ]
        });
        prompt.present();
      })
      .catch(function(error) {
        console.error("SMS not sent", error);
      });
  }
}
