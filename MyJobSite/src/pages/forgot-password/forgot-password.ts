import { Component } from "@angular/core";
import { NavController } from "ionic-angular";
import { Validators, FormGroup, FormControl } from "@angular/forms";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "forgot-password-page",
  templateUrl: "forgot-password.html"
})
export class ForgotPasswordPage {
  forgot_password: FormGroup;

  constructor(public nav: NavController, private auth: AuthService) {
    this.forgot_password = new FormGroup({
      email: new FormControl("", [Validators.required, Validators.email])
    });
  }

  recoverPassword() {
    if (!this.forgot_password.valid) {
      console.log(this.forgot_password.value);
    } else {
      console.log(this.forgot_password.value);
      this.auth.resetPassword(this.forgot_password.value.email);
    }
    console.log(this.forgot_password.value.email);
  }
}
