import { Component } from "@angular/core";
import {
  NavController,
  ModalController,
  AlertController,
  NavParams
} from "ionic-angular";
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { AuthService } from "../../services/auth.service";
import { HomePage } from "../home/home";
import { VerifyPhonePage } from "../verify-phone/verify-phone";

@Component({
  selector: "signup-page",
  templateUrl: "signup.html"
})
export class SignupPage {
  signup: FormGroup;
  createSuccess = false;
  main_page: { component: any };

  constructor(
    public nav: NavController,
    public modal: ModalController,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    fb: FormBuilder,
    private auth: AuthService
  ) {
    this.signup = new FormGroup({
      CompanyName: new FormControl(""),
      FName: new FormControl("", Validators.required),
      LName: new FormControl(""),
      Email: new FormControl("", [Validators.required, Validators.email]),
      Password: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      ConfirmPassword: new FormControl("", [
        Validators.required,
        Validators.minLength(6),
        this.equalto("Password")
      ])
    });
  }

  doSignup() {
    console.log(this.signup.value);
    let data = this.signup.value;

    if (!data.Email) {
      return;
    }
    let credentials = {
      email: data.Email,
      password: data.Password
    };
    // this.auth
    //   .signUp(credentials)
    //   .then(
    //     () => this.navCtrl.push(HomePage),
    //     error => this.showPopup("Error", error.message)
    //   );

    this.navCtrl.push(VerifyPhonePage);
  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: "OK",
          handler: data => {
            if (this.createSuccess) {
              this.navCtrl.popToRoot();
            }
          }
        }
      ]
    });
    alert.present();
  }

  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      let input = control.value;

      let isValid = control.root.value[field_name] == input;
      if (!isValid) {
        return { equalTo: { isValid } };
      } else return null;
    };
  }
}
